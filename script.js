 // 1.

const a = document.createElement('a');
const footerP = document.querySelector('footer p');

a.setAttribute('href', '#')
a.textContent = 'Learn More';

footerP.insertAdjacentElement('afterend', a);

// 2.

const features = document.querySelector('.features')
const select = document.createElement('select');

select.setAttribute('id', 'rating');

features.insertAdjacentElement('beforebegin', select);

const fragment = document.createDocumentFragment();

for (let i = 0; i < 4; i++) {
    const option = document.createElement('option');
    option.value = `${i + 1}`
    option.textContent = `${i + 1} Stars`;
    fragment.append(option);
}

console.log(fragment)
select.append(fragment);


