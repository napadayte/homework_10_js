### Теоретичні питання

1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?

   Для створення та додавання нових DOM-елементів в JavaScript можна використовувати наступні методи:

   - document.createElement(tagName): створює новий елемент з вказаним тегом.

   - element.append(child): додає елемент як останню дитину вказаного батьківського елемента.

   - element.insertBefore(newNode, referenceNode): вставляє новий елемент перед вказаним дочірнім елементом.

   - element.innerHTML: дозволяє вставляти HTML-код в елемент, створюючи нові елементи.

   - element.insertAdjacentHTML(position, text): вставляє текст у вказану позицію відносно елемента.


2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.

   Процес видалення елемента зі сторінки можна здійснити наступними кроками:

   - Крок 1: Знайти елемент з класом "navigation" на сторінці.

     let navigationElement = document.querySelector('.navigation');


   - Крок 2: Переконатися, що елемент існує.

     if (navigationElement) {
   - Крок 3: Видалити елемент з DOM
         navigationElement.remove();
     }

3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?

   - element.insertBefore(newNode, referenceNode): вставляє новий елемент перед вказаним дочірнім елементом.

   - element.insertAdjacentElement(position, newElement): вставляє новий елемент у вказану позицію відносно поточного елемента.
     - `beforebegin`: перед самим елементом.
     - `afterbegin`: всередині елемента, перед першим дочірнім елементом.
     - `beforeend`: всередині елемента, після останнього дочірнього елемента.
     - `afterend`: після самого елемента.

   - element.insertAdjacentHTML(position, text): вставляє HTML-код у вказану позицію відносно поточного елемента.

   - element.insertAdjacentText(position, text): вставляє текст у вказану позицію відносно поточного елемента.
